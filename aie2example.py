from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen


class GameLevel1Screen(Screen):
    pass


class LoginScreen(Screen):
    def is_valid_login(self, username, password):
        print("Login ->", username, password)

        message = self.ids["message"]
        if len(password) < 6:
            message.text = "Password length less than 6 alpahbets"
            message.color = (1, 1, 0, 1)
            return

        if username == password:
            message.text = "Login Pass"
            message.color = (0, 1, 0, 1)
            manager = super().manager
            manager.current = "level1"
            manager.transition.direction = "up"
        else:
            message.text = "Login Invalid"
            message.color = (1, 0, 0, 1)


class ExampleApp(App):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(LoginScreen(name="login"))
        sm.add_widget(GameLevel1Screen(name="level1"))

        return sm


if __name__ == "__main__":
    ExampleApp().run()
