from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.properties import NumericProperty, StringProperty
from kivy.core.window import Window
from kivy.lang.builder import Builder
import numpy as np
import itertools
import time


Window.size = (800,850)

class TTTButton(Button):
    def click(self):
        game_screen = self.parent.parent.parent.parent

        if game_screen.state % 2 == 0:
            self.text = "O"
            game_screen.state = 1
        else:
            self.text = "X"
            game_screen.state = 0

        self.disabled = True
        game_screen.click_count += 1

        if game_screen.click_count >= 5:
            game_screen.check_anwser()

    
class GameScreen(Screen):
    state = NumericProperty(0)
    click_count = NumericProperty(0)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        p = [[str(kk) + str(i) for i in range(3)] for kk in range(3)]
        [p.append(ii) for ii in (np.array(p).T).tolist()]
        p.append([p[i][i] for i in range(3)])
        p.append([p[2-i][i] for i in range(3)])
        self.p = p
        kp = p[:-2]
        pp = []
        [pp.extend(kp[i]) for i in range(len(kp))]
        self.pp = pp

    def check_anwser(self):
        print("Check answer")

        keys = ["".join(x) for x in itertools.product("012", repeat=2)]
        
        win_answers = self.p
        
        for i, j, k in win_answers:
            print(i, j, k)
            if self.ids[f"button_{i}"].text and (
                self.ids[f"button_{i}"].text
                == self.ids[f"button_{j}"].text
                == self.ids[f"button_{k}"].text
            ):
                print(self.ids[f"button_{i}"].text, "win")
                message = self.ids["message"]
                message.text = f'Player: {self.ids[f"button_{i}"].text} Win'
                message.color = (0, 1, 0, 1)
                for i in self.pp:
                    self.ids[f"button_{i}"].disabled = True
                break
        # for i in range(3):
        #     for j in range(3):
        #         print(self.ids[f"button_{i}{j}"].text)
    def restart(self):
        for ii in self.pp:
            self.ids[f"button_{ii}"].text = ""
            self.ids[f"button_{ii}"].disabled = False
            self.ids["message"].text = "Game Start"
            self.ids["message"].color = (1,1,1,1)
            self.state = 0

    def closeapp(self):
        app = App.get_running_app()
        app.stop()

class TicTacToeApp(App):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(GameScreen(name="game"))
        return sm


if __name__ == "__main__":
    TicTacToeApp().run()
